# 3. Laser-(pew)-cuter

create a simple 2D design with joints to interlock it with each other. 

##create the design:
click on sketch -> than on Modify -> lastly select 

![picture of the descrition above](../images/week03/change_parameters.png)

click on the *plus* and add the parameters you want to give your lines or whatever you create.

![picture of the descrition above](../images/week03/add.png)

to create now the line that you specified above, press **L** or just click on the create button. now start on **one point**, **but** instead of clicking again, **write the name** of the function and press **send**

![picture of the descrition above](../images/week03/create.png)

## my creation
[here](../files/week03/octav1.f3d) is the small jointy basic thing i have created

![picture of the descrition above](../images/week03/eye.png)

the engraving is supposed to be some kind of eye.

the engravings were created with circles while the sides&joints were lines with specified functions.

## exporting and colour coding

Once you export your .dxf file on an USB-stick, go to the pc next to the printer and open that file on there. Now go to the right and create Layers with colours. those will later be the specifications for the laser cuter. 

![picture of the pad](../images/week03/layer.jpg)

Now pick the lines and colour them on the Properties on the right. if you want to create a "raster" write "hatch" in the comand line and pic the area you choose, but do not forget to delete the boarder.

![picture of the UI on the PC of the Laser-Cuter](../images/week03/lines.jpg)

Press print or (Crtl+P) and move your project to the top left by selecting "set" in the "View and Output Scale" Tab. Following this press on "Properties" on the top left to open the Properties for the Laser-Cuter.

![picture of the pad](../images/week03/print.jpg)

Select Color Mapping on the top. Then create the colours and choose the Speed, Power and Freq. . Lastly move the order of what should be executed first (generally first the gravings and from the middle to the edge).

## the final object
This here is the object, printed 10 times, joined together.
![picture of the simple joint objects](../images/week03/eye.jpg)

![picture of the pad](../images/week03/properties.jpg)
## rules for/how to use the Laser Cutter
Only use it if you underwent the introduction by the staff.

This point will be repeated later BUT, never forget to use the air ventilator and the filter!

![picture of the pad](../images/week03/label.jpg)

If there are multiple jobs for the laser cuter you first need to select the one you want to "print" by moving the joystic up or down.

After that Focus the laser by puting on the plate that is next to the machine on the laser and move the pointer as low as the plate allows it. the plate has to barely touch the material! confirm by pressing the joystic!

Now, move by picking "Jog" the laser to the starting point, which should be the top left of the material to reduce waste. confirm by pressing the joystic!

Lastly pick the first menu point and before you start the cuting process, Turn on both the machines that are next to the machine!!

press on the green button and watch it go. Don't leave the laser cuter without supervision so in case of an emergency you can shut it down! 

After the job is done, keep the machines running for a few seconds to filter the air out. Once done turn the machines on the side off and take out everything that you inserted.

