# 1. Project Management

Introduction to Markdown, Git and the goals of this Course.

## Markdown
Markdown is a software to fomat text and with that have a simple way to create a modern Website

some simple markdown fomating tipps:

for a new paragraph you just let an empty line between texts.

for a Page-Header just use 1 (one) ```#```

for a header like the "markdown" one use 2 (two) ```##```

to make code structures more visible use " ``` " 3 times before and after the text

for pictures use this ```![Description that should be added](file / location)```

and lastly for hyperlinks use the one above but without the "!" ```[text that should be the hyperlink](link to the website)```

[click here to open the markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)



##Git
git is a version controlling website/service

here is a step by step tutorial. Without much text that explains unnecesary or long things.

first:

```
git vonfig --global user.name "YOURNAME"
```


```
git config --global user.email "Your.Email@Blank.xo"
``` 

after that you need a SSH key to clone/Copy the project from Git. But first create it with this:
``` 
ssh-keygen
``` 

now type 

```
git clone <Clone-with-SSH-URL>
``` 

now you should have the project in the folder that your Terminal is (check with ```ls```).


after editing stuff in your project, go to the folder and type

```
git status
```

now with this command you add the changes and allow them to be commited (just do it if you are sure that you want to get this saved)

```
git add .
```

after the add you need to commit the things you added


```
git commit -m "the-commit-message-you-should-wirte-to-see-the-changes-in-git"
```

now just type 

```
git push
```

and it should be done now.

[git cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf)

## reference

[markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[git cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf)