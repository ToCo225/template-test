# 5. Embedded Programming

This week I will use a button to increase the time that the LED on the arduino is turning on and stayingon for one more second than the last time.

## the setup
here is a picuture of the setup with the button.

![picture of the setup](../images/week05/button.jpg)

##problems
the problem is that the timeframe to register the button press is so small, that it needs to be held down.

##code
the code can also be downloaded below:
```
byte ledPin = 13;
byte buttpin = 7;        //button pin
boolean buttpress;
long n = 1000;
int x = 0;

void setup() {
  pinMode (buttpin, INPUT_PULLUP);
  pinMode (ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  buttpress = digitalRead(buttpin);
  digitalWrite(ledPin, LOW);
  Serial.println("led is off");
  delay(1000);                    //led is off for 1 seconds
  digitalWrite(ledPin, HIGH);
  Serial.print("led is on for: ");
  Serial.print(x);                //how many seconds is the light on
  Serial.print(" seconds");
  Serial.println();
  delay(n);
  if(!buttpress){                 //pressing the button
   Serial.println("button pushed");
   n = n+1000;                    //adds one seconds to the light
   x = n/1000;                    //adds one second to the text-output
  }
}
```
## video
demonstration of the code

[![youtube video that shows that the code is working](https://img.youtube.com/vi/8Ka5lPZ579o/0.jpg)](https://www.youtube.com/watch?v=8Ka5lPZ579o)

##sources
[the source code for the programm](../files/week05/button/button2.ino)