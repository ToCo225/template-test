# 6. Input Devices

In this Assignment i tried to get the moisture sensor running and working.

##the setup
the moisture sensor was installed like the turorial bellow mentioned.
https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193


![picture of the setup](../images/week06/moist.jpg)

##problems
the problem was that the monitor shoed an output that was very close betwenn holding the sensor in the air and sticking it in moist dirt.
different type of dirt need to be tested in the future.

cup of cold tea ~450<br>
air ~800 <br>
soil of my cactus ~785

![picture of the setup](../images/week06/dirt.png)


UPDATE:

it turned out that the dirt was too loose which meant that the sensor couldnt read the right amount of moisture in the dirt. 
##code

Old Version:

```
void setup() {
  Serial.begin(9600); // open serial port, set the baud rate as 9600 bps
}
void loop() {
  int val;
  val = analogRead(0); //connect sensor to Analog 0
  Serial.println(val); //print the value to serial port
  delay(100);
}

```

Improved Version:
```
const int dry = 800;
const int moist = 430;

void setup() {
  Serial.begin(9600); // open serial port, set the baud rate as 9600 bps
}
void loop() {
  int val;
  val = analogRead(0); //connect sensor to Analog 0
  int drymap = map(val, moist, dry, 100, 0);
  Serial.print(drymap); //print the value to serial port
  Serial.println("%");
  delay(100);
}
```
##sources

[the source code for the old programm version](../files/week06/moist/moist.ino)

[the source code for the new programm version](../files/week06/moist/moistv2.ino)
