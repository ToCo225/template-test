# 7. Output Devices

in this assignment I try to get a LCD display connected and working with an arduino.

##the setup
the display was connected with the arduino uno and the breadboard like this.

[![youtube video that shows the setup](https://img.youtube.com/vi/NbLOdwaFgPI/0.jpg)](https://www.youtube.com/watch?v=NbLOdwaFgPI)

this could have been done in a cleaner way, but without the proper equiment this is the solution i came up with. 
normaly you would connect the display direktly to the breadboard and have shorter cables connect with the positive and negatives. this would make cablemanagemnt a lot cleaner and easier to follow.



## problems
the potentionmeter is very sensetive, dialing up by around 10 percent makes the display unreadable.

##code

```
#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  lcd.begin(16, 2);
  lcd.print("hello, world!");
}

void loop() {
}
```

##sources

[the source code for the "hello, worldQ!](../files/week07/moist/lcd.ino)

[![youtube video that shows the setup](https://img.youtube.com/vi/Mr9FQKcrGpA/0.jpg)](https://www.youtube.com/watch?v=Mr9FQKcrGpA)

[website that showed me how to programm the LCD](https://www.circuitbasics.com/how-to-set-up-an-lcd-display-on-an-arduino/)