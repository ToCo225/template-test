# 2. 2D & 3D Design

this week we got an Introduction to 'LibreCAD' and 'Autodesk Fusion 360'

## Creating 2D Demos
i decided to test out LibreCAD by re-creating things i like. In my case its symbols from a Videogame called RuneScape.

Here are the 3 things: [file of magic](../files/magic.dxf), [melee](../files/melee.dxf), and [ranged](../files/ranged.dxf)

here is an example:

![melee picture of a sword](../images/week01/melee.png)


## Creating a 3D Stand
this stand would be something to hold the 3 Designs that were created with LibreCAD

[the f3d file](../files/standv1.f3d)

![picture of stand](../images/fusion_stand.png)

by creating a sketch first you can create a 3D Object