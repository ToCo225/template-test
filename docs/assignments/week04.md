# 4. 3D Printing

##making the 3D design
it was made with Fusion 360
![the sketch1](../images/week04/egg_sketch.png)
![the sketch2](../images/week04/revolve.png)

With the same method I created the little bowl outside the egg shaped object. Furthermore, with the use of the extrude tool I made a hole from the top to the bottom.   

![the sketch2](../images/week04/fusion.png)

##using slicer Cura
The Exported stl file was imported to the Ultimaker Cura.
After the settings (which are explained in the next paragraph) the object will be sliced.
![the sketch2](../images/week04/sliced.png)

In the preview section of the software you can check if support is needed or if there could be malfunctions. If everything is ready export the file on the bottom right of the screen and save it to a removable device.

##settings for the slicer
for our first 3D-Print we were only allowed to do something that would take 1Hour max. This meant to reduce the time by changing the settings of the Print.

The settings have been lost sadly.

But from memory the important things were lowering the quality by heightening the mm numbers, lower the infill from 20% to 15%-10%and lastly the speed was increased by a little bit to.

The first try was by increasing the speed by 2x, this resulted in a mistake.
![the sketch2](../images/week04/mistake.jpg)

after the mistake the speed was increased only by 5 to 10%.

Lastly the object was scaled down to 25% to reduce the printing time.   

##Using the Printer
Insert an USB-stick or another compatible device.

Check if there is material at the back of the printer, enough to print the object (you can check in the slicer cura program how much material you would need)

Make sure the surface is clean and there is nothing in the way of the printer moving inside. optionally you can add glue to the surface of the printer to make sure the object won't move while printing. 

Pick your file you moved on the device and stay there for the first layers to check if everything is going like planned.

IF something goes wrong just cancel the print with the menu and clean up.

##final product
![picture of it beeing printed](../images/week04/print1.jpg)

![the printed object](../images/week04/print2.jpg)

##downloadable files
[the Fusion 360 file](../files/week04/egg2.f3d)
[the stl file for cura](../files/week04/egg2.stl)

