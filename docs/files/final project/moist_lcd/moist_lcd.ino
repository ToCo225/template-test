#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int red_light_pin= 10;
int green_light_pin = 8;
int blue_light_pin = 9;

const int dry = 800;    //dirt is dry
const int moist = 430;  //dirst is too moist

void setup() {
  lcd.begin(16, 2);   //if this isnt inserted it will not work
  Serial.begin(9600); // open serial port, set the baud rate as 9600 bps
  
  pinMode(red_light_pin, OUTPUT);
  pinMode(green_light_pin, OUTPUT);
  pinMode(blue_light_pin, OUTPUT);
}
void loop() {
  lcd.setCursor(0, 0);  //lcd cursor at the beginning
  
  int val;
  val = analogRead(0); //connect sensor to Analog 0
  int drymap = map(val, moist, dry, 100, 0);  //generate a range from 0-100 on how moist the dirt is

  lcd.print("Insert in Dirt:");   //row one
  lcd.setCursor(0, 1);            //cahnge to second row of display
  lcd.print("moist:");
  lcd.print(drymap);              //print the moistness
  lcd.print("%");
  if(drymap < 50){                //if too dry -> red
    RGB_color(255, 0, 0); // Red
    }
  if(drymap >= 50 && drymap < 75){  //if a bit moist -> yellow
    RGB_color(255, 255, 0); // Yellow
    }
  if(drymap >= 75 && drymap < 85){  //if good moistered -> green
    RGB_color(0, 255, 0); // Green
    }
    if(drymap >=85) {               //if too moist -> purple
    RGB_color(200, 70, 255); //purple
    }
  delay(1000);
  lcd.clear();                      //resets the screen
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(red_light_pin, red_light_value);
  analogWrite(green_light_pin, green_light_value);
  analogWrite(blue_light_pin, blue_light_value);
 }
