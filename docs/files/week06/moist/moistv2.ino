const int dry = 800;
const int moist = 430;

void setup() {
  Serial.begin(9600); // open serial port, set the baud rate as 9600 bps
}
void loop() {
  int val;
  val = analogRead(0); //connect sensor to Analog 0
  int drymap = map(val, moist, dry, 100, 0);
  Serial.print(drymap); //print the value to serial port
  Serial.println("%");
  delay(100);
}
