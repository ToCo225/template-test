byte ledPin = 13;
byte buttpin = 7;        //button pin
boolean buttpress;
long n = 1000;
int x = 0;

void setup() {
  pinMode (buttpin, INPUT_PULLUP);
  pinMode (ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  buttpress = digitalRead(buttpin);
  digitalWrite(ledPin, LOW);
  Serial.println("led is off");
  delay(1000);                    //led is off for 1 seconds
  digitalWrite(ledPin, HIGH);
  Serial.print("led is on for: ");
  Serial.print(x);                //how many seconds is the light on
  Serial.print(" seconds");
  Serial.println();
  delay(n);
  if(!buttpress){                 //pressing the button
   Serial.println("button pushed");
   n = n+1000;                    //adds one seconds to the light
   x = n/1000;                    //adds one second to the text-output
  }
}
